#!/bin/bash

ASCIIDOC="asciidoc"
WKHTMLTOPDF="$HOME/usr/local/wkhtmltox/bin/wkhtmltopdf"
DOC=()
#DOC=("index.adoc" "OPENDATA.adoc" "PICO-8.adoc" )
DOC=("index.adoc" )

for (( I = 0; I < ${#DOC[@]}; ++I ))
do
#  $ASCIIDOC --backend slidy "${DOC[$I]}"
#  echo "mv ${DOC[$I]/\.adoc/\.html} ${DOC[$I]/\.adoc/_slide\.html}"
#  mv "${DOC[$I]/\.adoc/\.html}" "${DOC[$I]/\.adoc/_slide\.html}"
  $ASCIIDOC --backend html5 "${DOC[$I]}"
  $WKHTMLTOPDF "${DOC[$I]/\.adoc/\.html}" "${DOC[$I]/\.adoc/\.pdf}"
done
