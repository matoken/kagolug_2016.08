= 鹿児島Linux勉強会 2016.08
:author:    Kenichiro MATOHARA
:copyright: matoken
:presenter: Kenichiro MATOHARA
:twitter: matoken
:email: matoken@gmail.com
:web: http://matoken.org
:currentyear: 2016


include::about.adoc[]

include::OPENDATA.adoc[]

include::PICO-8.adoc[]
